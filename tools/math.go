package tools

import (
	"fmt"
	"strconv"
)

func Pow(x int, n int) int {
	ans := 1
	for n != 0 {
		ans *= x
		n--
	}
	return ans
}

func Money(inputMoney float64) float64 {
	price, err := strconv.ParseFloat(fmt.Sprintf("%.2f", inputMoney), 64)
	if err != nil {
		return 0.00
	} else {
		return price
	}
}
