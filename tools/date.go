package tools

import (
	"time"
)

func Now() *time.Time {
	now := time.Now()
	return &now
}

func GetDateYMDHIS(dateSep string, timeSep string, dateTimeSep string) string {
	now := time.Now()
	nowString := now.String()
	return nowString[0:4] + dateSep + nowString[5:7] + dateSep + nowString[8:10] + dateTimeSep + nowString[11:13] + timeSep + nowString[14:16] + timeSep + nowString[17:19]
}

func GetDateFromTimeYMDHIS(t time.Time, dateSep string, timeSep string, dateTimeSep string) string {
	nowString := t.String()
	return nowString[0:4] + dateSep + nowString[5:7] + dateSep + nowString[8:10] + dateTimeSep + nowString[11:13] + timeSep + nowString[14:16] + timeSep + nowString[17:19]
}

func GetDateYMD(cur ...time.Time) string {
	now := time.Now()
	if len(cur) > 0 {
		now = cur[0]
	}
	nowString := now.String()
	return nowString[0:4] + nowString[5:7] + nowString[8:10]
}

func GetDate(dateSep string) string {
	now := time.Now()
	nowString := now.String()
	return nowString[0:4] + dateSep + nowString[5:7] + dateSep + nowString[8:10]
}

func GetDateTime() string {
	return GetDateYMDHIS("-", ":", " ")
}

func TimeToString(time time.Time) string {
	return GetDateYMDHIS("-", ":", " ")
}

func GetTime(timeSep string) string {
	now := time.Now()
	nowString := now.String()
	return nowString[11:13] + timeSep + nowString[14:16] + timeSep + nowString[17:19]
}
func GetDateY() string {
	now := time.Now()
	nowString := now.String()
	return nowString[0:4]
}
func GetDateM() string {
	now := time.Now()
	nowString := now.String()
	return nowString[5:7]
}
func GetDateD() string {
	now := time.Now()
	nowString := now.String()
	return nowString[8:10]
}
func GetDayStart(cur time.Time) time.Time {
	newDate := time.Date(cur.Year(), cur.Month(), cur.Day(), 0, 0, 0, 0, time.Local)
	return newDate
}

func GetDayEnd(cur time.Time) time.Time {
	newDate := time.Date(cur.Year(), cur.Month(), cur.Day(), 0, 0, 0, 0, time.Local)
	newDate = newDate.AddDate(0, 0, 1)
	return newDate
}
func GetMonthStart(cur time.Time) time.Time {
	cst := time.FixedZone("CST", 8*3600)
	newDate := time.Date(cur.In(cst).Year(), cur.In(cst).Month(), 1, 0, 0, 0, 0, time.Local)
	return newDate
}

func China() *time.Location {
	return time.FixedZone("CST", 8*3600)
}
func ChinaSuffix() string {
	return "T00:00:00+08:00"
}

func GetMonthEnd(cur time.Time) time.Time {
	cst := time.FixedZone("CST", 8*3600)
	newDate := time.Date(cur.In(cst).Year(), cur.In(cst).Month()+1, 1, 0, 0, 0, -1, time.Local)
	return newDate
}

func AddTime(cur time.Time, h int, i int, s int) time.Time {
	//var prc, _ = time.LoadLocation("PRC")
	newDate := time.Date(cur.Year(), cur.Month(), cur.Day(), cur.Hour()+h, cur.Minute()+i, cur.Second()+s, 0, time.Local)
	return newDate
}

func GetDays(year int, month int) (days int) {
	if month != 2 {
		if month == 4 || month == 6 || month == 9 || month == 11 {
			days = 30

		} else {
			days = 31
		}
	} else {
		if ((year%4) == 0 && (year%100) != 0) || (year%400) == 0 {
			days = 29
		} else {
			days = 28
		}
	}
	return days
}
