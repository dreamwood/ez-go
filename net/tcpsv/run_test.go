package tcpsv

import (
	"fmt"
	"testing"
	"time"
)

func TestName(t *testing.T) {
	ch := make(chan string, 10)

	go func() {
		count := 0
		for {
			select {
			case i := <-ch:
				println("ch closed", i, count)
				time.Sleep(2 * time.Second)
				count++
			}
		}
	}()
	go func() {
		i := 0
		for {
			println(i)
			ch <- fmt.Sprintf("%d", i)
			time.Sleep(1 * time.Second)
			i++
			if i > 5 {
				return
			}
		}
	}()
	time.Sleep(8 * time.Second)
	println("end")
	close(ch)
	time.Sleep(15 * time.Second)
}
