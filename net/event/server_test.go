package event

import (
	"fmt"
	"testing"
	"time"
)

func TestServerAndClient(t *testing.T) {
	server := NewServer()
	server.Start()
	client := NewClient()
	client.Start()
	client.Subscribe("test", func(data interface{}) {
		println("client1 recv:", data, data.(string))
	})
	client1 := NewClient()
	client1.Start()
	client1.Subscribe("test", func(data interface{}) {
		println("client0 recv:", data, data.(string))
	})

	// go func() {
	// 	for {
	// 		time.Sleep(time.Second)
	// 		println("client send")
	// 		client.Ping()
	// 	}
	// }()
	for {

		println("====================")
		time.Sleep(time.Second)
		println("client1.ClientId:", fmt.Sprintf("%x", client1.ClientId))
		client1.Publish("test", "hello1")

		println("--------------------")
		client.Close()
		time.Sleep(time.Millisecond * 500)
		client.Start()
		time.Sleep(time.Millisecond * 500)
		println("client.ClientId:", fmt.Sprintf("%x", client.ClientId))
		client.Publish("test", "hello0")
		println("********************")
	}
}
