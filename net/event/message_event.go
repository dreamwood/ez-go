package event

import (
	"bytes"
	"encoding/gob"
)

type EventMessage struct {
	Topic string
	Data  interface{}
}

func (em *EventMessage) ToBytes() []byte {
	wr := bytes.NewBuffer([]byte{})
	nc := gob.NewEncoder(wr)
	nc.Encode(em)
	return wr.Bytes()
}
func (em *EventMessage) FromBytes(data []byte) {
	rd := bytes.NewReader(data)
	dc := gob.NewDecoder(rd)
	dc.Decode(em)
}
