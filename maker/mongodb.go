package maker

import "gitee.com/dreamwood/ez-go/maker/mongo"

func CreateDoc(name, cnName, dir string) *mongo.Document {
	return &mongo.Document{
		Name:   name,
		CnName: cnName,
		Path:   dir,
		Fields: make([]*mongo.Field, 0),
		Vars:   map[string]interface{}{},
	}
}
