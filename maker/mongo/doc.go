package mongo

import (
	"gitee.com/dreamwood/ez-go/ss"
)

type Document struct {
	Name         string
	CnName       string
	Path         string
	Fields       []*Field
	Relations    []*Field
	RelationDirs []string

	//特殊模型
	IsTree bool

	Vars ss.M
}

func (doc *Document) Add(name, cnName string) *Field {
	f := &Field{
		Name:   name,
		CnName: cnName,
	}
	doc.Fields = append(doc.Fields, f)
	return f
}

func (doc *Document) LoadTree() {
	doc.Add("sort", "排序").IsInt()
	doc.Add("l", "L").IsInt()
	doc.Add("r", "R").IsInt()
	doc.Add("level", "Level").IsInt()
	doc.Add("link", "link").IsString()
	doc.Add("parent", "父级菜单").IsJoinM2O(doc.Name)
	doc.Add("children", "子菜单").IsJoinO2M(doc.Name, "id", "ParentId")

	doc.IsTree = true
}

func (doc *Document) LoadStatus() {
	doc.Add("color", "字体颜色").IsString()
	doc.Add("background", "背景颜色").IsString()
	doc.Add("group", "分组").IsString()
	doc.Add("step", "阶段标识").IsString()
	doc.Add("warn", "警告标识").IsString()

	doc.IsTree = true
}

func (doc *Document) Generate() {
	//生成一些预处理的数据给模板使用
	relations := make([]*Field, 0)
	dirMap := make(ss.M)
	for _, field := range doc.Fields {
		if field.JoinDoc != "" {
			relations = append(relations, field)
		}
		if field.JoinDir != "" {
			dirMap[field.JoinDir] = 1
		}
	}
	doc.Relations = relations
	dirs := make([]string, 0)
	for dir, _ := range dirMap {
		dirs = append(dirs, dir)
	}
	doc.RelationDirs = dirs

	doc.CreateDocument()
	doc.CreateDocumentFunc()
	doc.CreateDocumentConfig()
	doc.CreateAutoController()
	doc.CreateAdminController()
	doc.CreateHttpRequest()
	doc.CreateVueAdminFile()
	doc.CreateDbManager()
	doc.CreateDocumentReg()
	wg.Wait()
}
