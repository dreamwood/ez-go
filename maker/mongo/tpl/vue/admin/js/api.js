import http from "@/assets/js/http";

export default {
    urlFind: '/__DIR__/admin/__EN_NAME__/get',
    urlFindToEdit: '/__DIR__/admin/__EN_NAME__/get',
    urlFindBy: '/__DIR__/admin/__EN_NAME__/list',
    urlSave: '/__DIR__/admin/__EN_NAME__/save',
    //urlDelete: '/__DIR__/admin/__EN_NAME__/delete',
    //urlDeleteAll: '/__DIR__/admin/__EN_NAME__/delete_many',
    urlDelete: '/__DIR__/admin/__EN_NAME__/destroy',
    urlDeleteAll: '/__DIR__/admin/__EN_NAME__/destroy_many',
    urlCopy: '/__DIR__/admin/__EN_NAME__/copy',
    urlEditMany: '/__DIR__/admin/__EN_NAME__/edit_many',
    urlTree: '/__DIR__/admin/__EN_NAME__/tree',
    urlChoice: '/__DIR__/admin/__EN_NAME__/choice',
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}