import ezAuth from "@/assets/js/ez-auth";

export default {
    data() {
        return {
            auth_add: ezAuth.reg("+__DIR__.__EN_NAME__.add", "__CN_NAME___新增"),//新增
            auth_copy: ezAuth.reg("+__DIR__.__EN_NAME__.copy", "__CN_NAME___复制"),//复制
            auth_edit: ezAuth.reg("+__DIR__.__EN_NAME__.edit", "__CN_NAME___编辑"),//编辑
            auth_del: ezAuth.reg("-__DIR__.__EN_NAME__.del", "__CN_NAME___删除"),//删除
            auth_view: ezAuth.reg("+__DIR__.__EN_NAME__.view", "__CN_NAME___查看"),//查看
        }
    },
    directives: {
        auth: {
            bind: ($el, binding, vnode, prevVnode) => {
                ezAuth.checkAllow(binding.value) ? $el.style.display = "normal" : $el.style.display = "none"
            }
        }
    }
}