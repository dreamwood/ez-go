import formBuilder from "@/comps/form/formBuilder";

export default function () {
    let fb = formBuilder()
    fb.setLabelPosition("right").setLabelWidth(100)
    fb.addText("name").setLabel("名称").setSpan(3).setTips("名称模糊搜索")
    return fb
}