import confEditMany from "./conf_edit_many";
import apis from "./api";
export default {
    data(){
        return{
            //是否显示批量编辑表单
            showEdit:false,
            //筛选器
            editData: {},
            editFormConfig:confEditMany(),
            editFormUpdateKey: Math.random(),
        }
    },
    methods:{
        edit() {
            let ids = []
            for (let row of this.selected) {
                ids.push(this.data[row].id)
            }
            for (let editDataKey in this.editData) {
                if (!this.editData[editDataKey]){
                    this.editData = this.$utils.objDelIndex(this.editData,editDataKey)
                }
            }
            apis.editMany(ids, this.editData, res => {
                this.$toast.info(
                    res.message
                )
                this.getList(this.page)
            })
        },

        resetEditData() {
            this.editData = {}
            this.editFormUpdateKey = Math.random()
        },
    },
    watch: {
        showEdit() {
            if (this.showEdit) {
                this.showSearch = false
            }
        }
    }
}