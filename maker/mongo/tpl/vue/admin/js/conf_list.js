import listBuilder from "@/comps/list/listBuilder";

/*
__LIST_BUILDER__
*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    return lb.headers
}