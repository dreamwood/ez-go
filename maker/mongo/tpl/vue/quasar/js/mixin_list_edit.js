export default {
  data(){
    return {
      showEdit:false,
      curEditId:0,
      editUpdateKey:"",
      saveOnHide:false,
    }
  },
  methods:{
    openEdit(id){
      if (id > 0){
        this.curEditId = id
      }else {
        this.curEditId = 0
      }
      this.editUpdateKey = Math.random()
      this.showEdit = true
    },
    onEditClose(){
      this.getList()
    },
    saveBeforeHide(){
      if (this.saveOnHide){
        this.$confirm.info("正在退出编辑，是否保存当前信息","", d=>{
          this.$refs.miniEditor.save()
        },d=>{
          this.showEdit = false
        },"保存","直接关闭")
      }else {
        this.showEdit = false
      }
    }
  }
}
