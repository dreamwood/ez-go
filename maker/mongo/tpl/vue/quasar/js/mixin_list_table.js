import listBuilder from "components/list/listBuilder";
import IcsStore from "boot/IcsStore";

export default {
  data(){
    let lb = listBuilder()
    lb.add("id", "#",100)
    /**------------------------可编辑区域Start-----------------------------**/

    /**------------------------可编辑区域End-----------------------------**/
    lb.addAction(100).setListAlignRight()

    let visibleColumns = []
    for (const head of lb.headers) {
      visibleColumns.push(head.name)
    }

    let store = IcsStore()
    store.useNetwork().setKeyPrefix("ics.__DIR__.__EN_NAME__")
    return{
      listBuilder:lb,
      table:{
        columns:lb.headers,
        visibleColumns,
        fullscreen:false,
      },
      store,
      page:{
        page:1,
        size:10,
        total:10,
        sizeChoice:[5,10,20,50,100,200,500],
        showPageSizeInput:false,
        rowsNumber:0,
      },
      selected:[],
      conf_btn_placement:{
        anchor:"top middle",
        self:"center middle",
        offset:[0,20],
        'content-class':"text-body2",
      },
      conf_btn_placement_err:{
        anchor:"top middle",
        self:"center middle",
        offset:[0,20],
        'content-class':"text-body2 bg-negative text-white",
      }
    }
  },
  mounted() {
    if (this.conf_cache_enable_table_header){
      this.store.get(this.store.key("table.headers"),res=>{
        this.table = res
      },true)
    }
  },
  methods:{
    clearCache(){
      let visibleColumns = []
      let headers = this.listBuilder.headers
      for (const head of headers) {
        visibleColumns.push(head.name)
      }
      this.table = {
        columns:headers,
        visibleColumns:visibleColumns,
        fullscreen:false,
      }
      if (this.conf_cache_enable_table_header){
        this.store.set(this.store.key("table.headers"), this.table)
      }
    },
    rebuildVisibleColumns(){
      let tmp = []
      for (let column of this.table.columns) {
        if (column.show){
          tmp.push(column.name)
        }
      }
      this.table.visibleColumns = tmp
      if (this.conf_cache_enable_table_header){
        this.store.set(this.store.key("table.headers"), this.table)
      }
    },
    showSelectCount(){
      return this.selected.length === 0 ? '' : `当前页共${this.data.length}项，选中${this.selected.length}项`
    }
  },
  watch:{
    'page.size':{
      handler(val){
        if (val > 999){
          this.$toast.error('数据量过大会导致浏览器卡顿，请调整分页大小')
        }
      }
    }
  }
}
