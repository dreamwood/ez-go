import {RP, RB, RD} from "src/router/register";

const ModelName = "__EN_NAME__";
const ModelCnName = "__CN_NAME__";
export default [
  RD(`${ModelName}`,`${ModelCnName}管理`).add(
    RP(`List`, `${ModelCnName}_列表`)
      .add(
        RB('新增', 'add+'),
        RB('删除', 'del-'),
        RB('编辑', 'edit+'),
        RB('复制', 'copy-'),
      ),
  ),
]

