import http from "boot/http";
import {root} from "../../config"
const ModelName = "__EN_NAME__"
export default {
  urlGet:`/${root}/admin/${ModelName}/get`,
  urlList:`/${root}/admin/${ModelName}/list`,
  urlSave:`/${root}/admin/${ModelName}/save`,
  urlCopy:`/${root}/admin/${ModelName}/copy`,
  urlChoice:`/${root}/admin/${ModelName}/choice`,
  urlDelete:`/${root}/admin/${ModelName}/delete`,
  urlDeleteMany:`/${root}/admin/${ModelName}/delete_many`,
  urlExport:`/${root}/admin/${ModelName}/export`,
  urlImport:`/${root}/admin/${ModelName}/import`,

  apiGet(param,callback){
    http.Post(this.urlGet,param,callback)
  },
  apiList(param,callback){
    http.Post(this.urlList,param,callback)
  },
  apiSave(param,callback){
    http.Post(this.urlSave,param,callback)
  },
  apiDelete(id,callback){
    http.Post(this.urlDelete,{id},callback)
  },
  apiDeleteMany(ids,callback){
    http.Post(this.urlDeleteMany,{ids},callback)
  },
  apiCopy(ids,callback){
    http.Post(this.urlCopy,{ids},callback)
  },
  apiExport(param,callback){
    http.Post(this.urlExport,param,callback)
  },
  apiImport(param,callback){
    http.Post(this.urlImport,param,callback)
  },
}
