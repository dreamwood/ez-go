import api from "./api";
import formBuilder from "components/form/formBuilder";

export default {
  data() {
    let searchForm = formBuilder()
    searchForm.setLabelWidth(96)

    return {
      loading: false,
      data: [],
      showSearch: false,
      searchModel:{},
      searchForm,
      searchFormUpdateKey:"",
      searchText:"",
      conf_sort:[
        {label: "ID", key: "id"},
      ],
      orderModel:["-id"],
      dumps:[],
      conf_import_url:api.urlImport,
      conf_export_url:api.urlExport,
    }
  },
  mounted() {
    this.getList()
  },
  methods: {
    rebuild(){
      api.apiRebuild()
    },
    getList(page) {
      this.loading = true
      if (page > 0) this.page.cur = page
      let where = {...this.searchModel}
      if (this.editBase !== undefined){
        Object.assign(where,this.editBase)
      }
      if (this.queryBase !== undefined){
        Object.assign(where,this.queryBase)
      }
      if (this.searchText !== null && this.searchText.length > 0  ){
        where["_or"] = [
          {id:this.searchText*1}
        ]
      }
      api.apiList({
        _where: where,
        page: this.page.page,
        limit: this.page.size,
        _order: this.orderModel,
        _dumps: this.dumps,
      }, res => {
        this.loading = false
        this.data = res.data.lists
        this.page.page = res.data.query.page
        this.page.total = res.data.query.total
        this.page.max = Math.ceil(res.data.query.total / this.page.size)
      })
    },
    deleteOne(id) {
      this.$confirm.error("警告", "正在删除数据,请确认", d => {
        api.apiDelete(id, res => {
          this.$toast.success("删除")
          this.getList()
        })
      }, d => {
        this.$toast.warn("取消", "取消删除")
      })
    },
    deleteMany(){
      this.$confirm.error("警告", "正在删除数据,请确认", d => {
        let select = []
        for (const selectedElement of this.selected) {
          select.push(selectedElement.id)
        }
        if (select.length === 0){
          this.$toast.error("未选择任何项目")
          return
        }
        api.apiDeleteMany(select, res => {
          this.$toast.success("删除")
          this.page.page = 1
          this.getList(1)
        })

      }, d => {
        this.$toast.warn("取消", "取消删除")
      })
    },
    copy(){
      let select = []
      for (const selectedElement of this.selected) {
        select.push(selectedElement.id)
      }
      if (select.length === 0){
        this.$toast.error("未选择任何项目")
        return
      }
      api.apiCopy(select,res=>{
        this.selected = []
        this.$toast.success("复制成功")
        this.getList()
      })
    },
    resetSearchBox(){
      this.searchModel={}
      this.searchFormUpdateKey=Math.random()
      this.getList()
    },
    search(){
      this.showSearch = false
      this.getList()

    },
    reSort(_order){
      this.orderModel = _order
      this.getList()
    }
  },
  watch: {
    'page.size': {
      handler(val) {
        if (val > 999) {
          this.$toast.error('数据量过大会导致浏览器卡顿，请调整分页大小')
        }
      }
    }
  }
}
