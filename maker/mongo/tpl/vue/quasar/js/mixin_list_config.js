import {root, getPermission} from '../../config'
const model = "__EN_NAME__"
export default {
  data(){
    return{
      conf_model_name:model,
      conf_title:"__CN_NAME__",
      conf_edit_mini:true,
      conf_cache_enable_table_header:false,
      useSearch:false,
      permission:{
        add:`${root}.${model}.List.add+`,
        del:`${root}.${model}.List.del-`,
        edit:`${root}.${model}.List.edit+`,
        copy:`${root}.${model}.List.copy-`,
      }
    }
  },
  directives: {
    auth: {
      bind: ($el, binding, vnode, prevVnode) => {
        getPermission(binding.value) ? $el.style.display = "normal" : $el.style.display = "none"
      }
    }
  }
}
