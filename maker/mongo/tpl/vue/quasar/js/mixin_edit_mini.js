import formBuilder from "components/form/formBuilder";
import api from "./api"
export default {
  props:{
    modelId:{
      default(){
        return 0
      }
    },
    editBase:{
      type:Object,
      default(){
        return {}
      }
    },
  },
  data(){
    let fb = formBuilder()
    fb.setLabelWidth(96)
    /**------------------------可编辑区域Start-----------------------------**/

    /**------------------------可编辑区域End-----------------------------**/
    return{
      formBuilder: fb,
      editModel: {},
      editFormUpdateKey:""
    }
  },
  mounted() {
    if (this.modelId > 0){
      api.apiGet({id:this.modelId},res=>{
        this.editModel = res.data
        this.editFormUpdateKey = Math.random()
      })
    }else {
      this.editModel = this.editBase === undefined?{} : {...this.editBase}
      this.editFormUpdateKey = Math.random()
    }
  },
  methods:{
    save(){
      api.apiSave(this.editModel,res=>{
        if (res.code > 2001){
          this.$toast.error("ERROR",res.message)
        }else {
          // this.$confirm.info("是否留在此页继续编辑？", "否：返回列表；是：留在此页。", d => {
          //   this.editModel = res.data
          // }, d => {
          //   this.$emit("done")
          // })
          this.$emit("done")
        }
      })
    },
    cancel(){
      this.$emit("done")
    }
  }
}
