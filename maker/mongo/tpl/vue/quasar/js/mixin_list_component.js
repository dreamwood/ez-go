export default {
  props:{
    editBase:{
      type:Object,
      default(){
        return {}
      }
    },
    queryBase:{
      type:Object,
      default(){
        return {}
      }
    },
  },
  data(){
    return {
      isInComponent:false,
    }
  }
}
