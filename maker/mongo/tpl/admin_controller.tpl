package controller

import (
	"ics/apps/{{.Path}}/auto/controller"
	"gitee.com/dreamwood/ez-go/ez"
)

type {{.Name}}Controller struct {
	controller.{{.Name}}AutoController
}

func init() {
	c := &{{.Name}}Controller{}
	c.SetRouteParam("/{{.Path}}", "/admin","{{.Name}}")
	ez.CreateApi(c.AdminGet(), c.GetAction).SetRouterName("[{{.Path}}]{{.CnName}}_创建")
	ez.CreateApi(c.AdminList(), c.ListAction).SetRouterName("[{{.Path}}]{{.CnName}}_列表")
	ez.CreateApi(c.AdminSave(), c.SaveAction).SetRouterName("[{{.Path}}]{{.CnName}}_保存")
	ez.CreateApi(c.AdminCopy(), c.CopyAction).SetRouterName("[{{.Path}}]{{.CnName}}_复制")
	ez.CreateApi(c.AdminUpdate(), c.UpdateAction).SetRouterName("[{{.Path}}]{{.CnName}}_更新")
	ez.CreateApi(c.AdminChoice(), c.ChoiceAction).SetRouterName("[{{.Path}}]{{.CnName}}_选项")
	ez.CreateApi(c.AdminDelete(), c.DeleteAction).SetRouterName("[{{.Path}}]{{.CnName}}_删除")
	ez.CreateApi(c.AdminUnDelete(), c.UnDeleteAction).SetRouterName("[{{.Path}}]{{.CnName}}_恢复")
	ez.CreateApi(c.AdminDestroy(), c.DestroyAction).SetRouterName("[{{.Path}}]{{.CnName}}_销毁")
	ez.CreateApi(c.AdminUpdateMany(), c.UpdateManyAction).SetRouterName("[{{.Path}}]{{.CnName}}_更新_批量")
	ez.CreateApi(c.AdminDeleteMany(), c.DeleteManyAction).SetRouterName("[{{.Path}}]{{.CnName}}_删除_批量")
	ez.CreateApi(c.AdminDestroyMany(), c.DestroyManyAction).SetRouterName("[{{.Path}}]{{.CnName}}_销毁_批量")
	ez.CreateApi(c.AdminImport(), c.ImportAction).SetRouterName("[mes]{{.CnName}}_批量导入")
	ez.CreateApi(c.AdminExport(), c.ExportAction).SetRouterName("[mes]{{.CnName}}_批量导出")
{{if eq .IsTree true}}
ez.CreateApi(c.AdminTree(), c.TreeAction).SetRouterName("[{{.Path}}]{{.CnName}}_选项_树形")
{{end}}
}

//func (c {{.Name}}Controller) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
//func (c {{.Name}}Controller) GetAction(session *ez.Session) { /* 在这里面重构 */ }
//func (c {{.Name}}Controller) ListAction(session *ez.Session) { /* 在这里面重构 */ }
