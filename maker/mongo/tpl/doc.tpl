package {{.Path}}

import (
	"context"
	"encoding/json"
	"ics/apps/{{.Path}}/auto/mc"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"time"
{{ range $dir := .RelationDirs}}	. "ics/apps/{{$dir}}/document"
{{end}}
)

type {{.Name | ucfirst}} struct {
	mgo.BaseDoc `bson:"-" json:"-"`
	Id          int64 `json:"id" bson:"id,omitempty"`
{{range $f := .Fields}}
{{if ne $f.JoinType ""}}
{{$f.Name | ucfirst}} {{$f.Type}} `json:"{{$f.Name | lcfirst}}" bson:"{{$f.Name | lcfirst}}"`//{{$f.CnName}}
{{if eq $f.JoinType "MM"}}{{$f.Name | ucfirst}}Ids []int64 `json:"{{$f.Name | lcfirst}}Ids" bson:"{{$f.Name | lcfirst}}Ids"`//{{$f.CnName}}{{end}}
{{if eq $f.JoinType "O"}}{{$f.Name | ucfirst}}Id int64 `json:"{{$f.Name | lcfirst}}Id" bson:"{{$f.Name | lcfirst}}Id"`//{{$f.CnName}}{{end}}
{{else}}
{{$f.Name | ucfirst}} {{$f.Type}} `json:"{{$f.Name | lcfirst}}" bson:"{{$f.Name | lcfirst}}"`//{{$f.CnName}}
{{end}}
{{end}}
	CreateAt    time.Time          `json:"createAt" bson:"createAt"`
	UpdateAt    time.Time          `json:"updateAt" bson:"updateAt"`
	DeleteAt    *time.Time         `json:"deleteAt" bson:"deleteAt"`
}

func (this *{{.Name | ucfirst}}) DocName() string { return "{{.Name | ucfirst}}" }

func (this *{{.Name | ucfirst}}) GetId() int64 { return this.Id }

func (this *{{.Name | ucfirst}}) SetId(id int64) { this.Id = id }

func (this *{{.Name | ucfirst}}) Create() error {
	return this.GetFactory().Create(this)
}

func (this *{{.Name | ucfirst}}) Replace() error {
	return this.GetFactory().Replace(this)
}

func (this *{{.Name | ucfirst}}) Save() error {
	if this.Id == 0 {
		this.CreateAt = time.Now()
		this.UpdateAt = time.Now()
		return this.GetFactory().Create(this)
	} else {
		this.UpdateAt = time.Now()
		return this.GetFactory().Replace(this)
	}
}

// 伪删除
func (this *{{.Name | ucfirst}}) Delete() error {
	return this.GetFactory().Delete(this)
}

func (this *{{.Name | ucfirst}}) UnDelete() error {
	return this.GetFactory().UnDelete(this)
}

// 真删除
func (this *{{.Name | ucfirst}}) Destroy() error {
	return this.GetFactory().Destroy(this)
}

func (this *{{.Name | ucfirst}}) ToString() string {
	return string(this.ToBytes())
}

func (this *{{.Name | ucfirst}}) ToBytes() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *{{.Name | ucfirst}}) Serialize() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *{{.Name | ucfirst}}) UnSerialize(data []byte) []byte {
	ez.Try(json.Unmarshal(data, this))
	return data
}
{{range $f := .Fields}}
{{if eq $f.JoinType "MM"}}
func (this *{{$.Name | ucfirst}}) Load{{$f.Name | ucfirst}}() {
	if this.{{$f.Name | ucfirst}} == nil {
		this.{{$f.Name | ucfirst}} = make([]*{{$f.JoinDoc}}, 0)
	}
	if this.{{$f.Name | ucfirst}}Ids == nil {
		this.{{$f.Name | ucfirst}}Ids = make([]int64, 0)
	}
	if len(this.{{$f.Name | ucfirst}}Ids) > 0 {
		this.{{$f.Name | ucfirst}} = make([]*{{$f.JoinDoc}}, 0)
		for _, v := range this.{{$f.Name | ucfirst}}Ids {
			find, e := New{{$f.JoinDoc}}Crud().FindId(v)
			if e == nil {
				this.{{$f.Name | ucfirst}} = append(this.{{$f.Name | ucfirst}}, find)
			}
		}
	}
}
{{end}}
{{if eq $f.JoinType "O"}}
func (this *{{$.Name | ucfirst}}) Load{{$f.Name | ucfirst}}() {
	if this.{{$f.Name | ucfirst}}Id == 0{
		return
	}
	this.{{$f.Name | ucfirst}}, _ = New{{$f.JoinDoc}}Crud().FindId(this.{{$f.Name | ucfirst}}Id)
}
{{end}}
{{if eq $f.JoinType "M"}}
func (this *{{$.Name | ucfirst}}) Load{{$f.Name | ucfirst}}() {
	{{$f.Name | lcfirst}}, _ := New{{$f.JoinDoc}}Crud().FindBy(ss.M{"{{$f.KeyOutSide | lcfirst}}": this.Id}, []string{"id"}, 0, 0)
	this.{{$f.Name | ucfirst}} = {{$f.Name | lcfirst}}
}
{{end}}
{{end}}

func (this *{{$.Name | ucfirst}}) ClearRelationsBeforeSave()mgo.Doc{
{{range $f := .Relations}}
this.{{$f.Name | ucfirst}} = nil
{{end}}
	return this
}

func neverUsed_{{$.Name | ucfirst}}(){
	//导入ss包
	a := ss.M{}
	ez.Debug(a)
}


type {{.Name}}AccessControl struct {
	Access  bool
	Message string
	Action  string //控制器Action,小写开头，如c,u,r,d
	Model   *{{.Name}}
	Session *ez.Session
}

func New{{.Name}}AccessControl(model *{{.Name}}, action string, session *ez.Session) *{{.Name}}AccessControl {
	ctrl := &{{.Name}}AccessControl{
		Access:  true,
		Model:   model,
		Action:  action,
		Session: session,
	}
	ez.DispatchToMany(mc.{{.Name}}AccessControlEvent, ctrl, context.TODO())
	return ctrl
}
