package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
{{ range $dir := .RelationDirs}}	. "ics/apps/{{$dir}}/auto/mc"
{{end}}
)

const (
{{.Name}}EventNew = "{{.Path}}.{{.Name}}New"
{{.Name}}EventBeforeCreate = "{{.Path}}.{{.Name}}BeforeCreate"
{{.Name}}EventBeforeUpdate = "{{.Path}}.{{.Name}}BeforeUpdate"
{{.Name}}EventBeforeSave = "{{.Path}}.{{.Name}}BeforeCreate {{.Path}}.{{.Name}}BeforeUpdate"
{{.Name}}EventAfterCreate = "{{.Path}}.{{.Name}}AfterCreate"
{{.Name}}EventAfterUpdate = "{{.Path}}.{{.Name}}AfterUpdate"
{{.Name}}EventAfterSave = "{{.Path}}.{{.Name}}AfterCreate {{.Path}}.{{.Name}}AfterUpdate"
{{.Name}}EventDelete = "{{.Path}}.{{.Name}}Delete"

{{.Name}}AccessControlEvent = "{{.Path}}.{{.Name}}AccessControl"
)


func Get{{.Name}}Config() *mgo.DocConfig {
	return {{.Name}}_Config
}

var {{.Name}}_Config *mgo.DocConfig

func init() {
	{{.Name}}_Config = New{{.Name}}Config()
}

func New{{.Name}}Config() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "{{.Path}}.{{.Name}}",
		Fields: []string{
			{{range $f := .Fields}}"{{$f.Name | lcfirst}}",{{end}}
		},
		RelationFields: []string{
			{{range $f := .Relations}}"{{$f.Name | lcfirst}}",{{end}}
		},
		RelationConfigs: map[string]*mgo.DocRelation{
{{range $r := .Relations}}
"{{$r.Name | lcfirst}}": {
Config:     Get{{.JoinDoc}}Config,
DocName:    "{{.JoinDoc}}",
JoinType:   "{{$r.JoinType}}",
KeyInside:  "{{$r.KeyInside | lcfirst}}",
KeyOutSide: "{{$r.KeyOutSide | lcfirst}}",
},
{{end}}
		},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
		ExcelFields: map[string]*mgo.ExcelField{
			"default": {
				[]string{ {{range $f := .Fields}}"{{$f.Name | lcfirst}}{{if eq $f.JoinType "O"}}Id{{end}}",{{end}}"id" },
				[]string{ {{range $f := .Fields}}"{{$f.CnName}}",{{end}}"ID" },
			},
		},
	}
}
