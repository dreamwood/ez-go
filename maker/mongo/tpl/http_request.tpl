### get
POST {{.Vars.host}}/{{.Path}}/admin/{{.Name}}/get
Content-Type: application/json

{
  "id": 0
}

### copy
POST {{.Vars.host}}/{{.Path}}/admin/{{.Name}}/copy
Content-Type: application/json

{
  "id": 0
}

### delete
POST {{.Vars.host}}/{{.Path}}/admin/{{.Name}}/delete
Content-Type: application/json

{
  "id": 0
}

### undelete
POST {{.Vars.host}}/{{.Path}}/admin/{{.Name}}/undelete
Content-Type: application/json

{
  "id": 0
}

### delete_many
POST {{.Vars.host}}/{{.Path}}/admin/{{.Name}}/delete_many
Content-Type: application/json

{
  "ids": [0]
}

### destroy
POST {{.Vars.host}}/{{.Path}}/admin/{{.Name}}/destroy
Content-Type: application/json

{
  "id": 0
}

### destroy_many
POST {{.Vars.host}}/{{.Path}}/admin/{{.Name}}/destroy_many
Content-Type: application/json

{
  "ids": [0]
}

### save
POST {{.Vars.host}}/{{.Path}}/admin/{{.Name}}/save
Content-Type: application/json

{
  "Ip": "Ip"
}

### update
POST {{.Vars.host}}/{{.Path}}/admin/{{.Name}}/update
Content-Type: application/json

{
  "id": 0,
  "model": {
    "Field": "Field"
  }
}

### list
POST {{.Vars.host}}/{{.Path}}/admin/{{.Name}}/list
Content-Type: application/json

{
  "_where":{
  },
  "_order":["-id"],
  "page":1,
  "limit":10,
  "_dumps":[]
}


### choice
POST {{.Vars.host}}/{{.Path}}/admin/{{.Name}}/choice
Content-Type: application/json

{
  "_where":{
  },
  "_order":["-id"],
  "page":1,
  "limit":1000
}
