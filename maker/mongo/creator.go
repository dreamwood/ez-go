package mongo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitee.com/dreamwood/ez-go/tools"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
	"text/template"
)

var wg sync.WaitGroup

func (doc *Document) CreateDocumentReg() {

	targetPath := fmt.Sprintf("../auto/reg/document/%s.json", doc.Name)

	fullPath, e := filepath.Abs(targetPath)
	if e != nil {
		println(e)
	}
	_ = tools.CreateDirForPath(fullPath)
	fileData, _ := json.Marshal(doc)
	tools.CreateFile(targetPath, fileData)
}
func (doc *Document) CreateDocument() {
	tpl := "/tpl/doc.tpl"
	targetPath := fmt.Sprintf("../document/%s_.go", doc.Name)
	doc.Render(tpl, targetPath)
}
func (doc *Document) CreateDocumentConfig() {
	tpl := "/tpl/config.tpl"
	targetPath := fmt.Sprintf("../auto/mc/%s_Config.go", doc.Name)
	doc.Render(tpl, targetPath)
}
func (doc *Document) CreateDocumentFunc() {
	tpl := "/tpl/funcs.tpl"
	targetPath := fmt.Sprintf("../document/%s_Func.go", doc.Name)
	doc.RenderOnce(tpl, targetPath)
}
func (doc *Document) CreateAutoController() {
	tpl := "/tpl/auto_controller.tpl"
	targetPath := fmt.Sprintf("../auto/controller/%sAutoController.go", doc.Name)
	doc.Render(tpl, targetPath)
}
func (doc *Document) CreateAdminController() {
	tpl := "/tpl/admin_controller.tpl"
	targetPath := fmt.Sprintf("../controller/admin/%sAdminController.go", doc.Name)
	doc.RenderOnce(tpl, targetPath)
}
func (doc *Document) CreateHttpRequest() {
	tpl := "/tpl/http_request.tpl"
	targetPath := fmt.Sprintf("../auto/http/%s.http", doc.Name)
	doc.Vars["host"] = "{{ host }}"
	doc.RenderOnce(tpl, targetPath)
}
func (doc *Document) CreateDbManager() {
	tpl := "/tpl/db_drop.tpl"
	targetPath := fmt.Sprintf("../auto/db/%s_test.go", doc.Name)
	doc.RenderOnce(tpl, targetPath)
}

func (doc *Document) Render(tplPath string, toPath string) {
	var dir string
	_, filename, _, ok := runtime.Caller(0)
	if ok {
		dir = path.Dir(filename)
	}
	content, err := os.ReadFile(dir + tplPath)
	t := template.Must(template.New("CreateDocument").Funcs(template.FuncMap{
		"ucfirst": func(in string) string {
			return strings.ToUpper(in[0:1]) + in[1:]
		},
		"lcfirst": func(in string) string {
			return strings.ToLower(in[0:1]) + in[1:]
		},
	}).Parse(string(content)))

	bf := bytes.NewBufferString("")
	err = t.Execute(bf, doc)
	if err != nil {
		log.Println("executing template:", err)
	}
	fullPath, e := filepath.Abs(toPath)
	if e != nil {
		println(e)
	}
	tools.CreateDirForPath(fullPath)
	fileData := bf.String()
	if path.Ext(toPath) == ".go" {
		i := 0
		for {
			fileData = strings.ReplaceAll(fileData, "\r\n\r\n", "\r\n")
			i++
			if i > 5 {
				break
			}
		}
	}
	tools.CreateFile(toPath, []byte(fileData))
	if path.Ext(toPath) == ".go" {
		wg.Add(1)
		go func() {
			e = exec.Command("go", "fmt", toPath).Run()
			if e != nil {
				println("go fmt 5", e.Error())
			}
			wg.Done()
		}()
	}

}
func (doc *Document) RenderOnce(tplPath string, toPath string) {
	if tools.FileExist(toPath) {
		return
	}
	doc.Render(tplPath, toPath)
}
