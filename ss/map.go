package ss

import (
	"encoding/json"
)

type M map[string]interface{}

func (s M) Len() int              { return len(s) }
func (s M) Swap(i, j string)      { s[i], s[j] = s[j], s[i] }
func (s M) Less(i, j string) bool { return i < j }
func (s *M) String() string       { data, _ := json.Marshal(s); return string(data) }
