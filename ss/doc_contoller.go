package ss

type DocIds struct {
	Ids []int64 `json:"ids"`
}

func NewIds() *DocIds {
	return &DocIds{make([]int64, 0)}
}

type DocUpdater struct {
	Id    int64   `json:"id"`
	Ids   []int64 `json:"ids"`
	Model M       `json:"model"`
}

func NewDocUpdater() *DocUpdater {
	return &DocUpdater{}
}

type Counter struct {
	Count int64 `bson:"count"`
}
