package ez

import (
	"fmt"
	"gitee.com/dreamwood/ez-go/tools"
	"os"
	"runtime"
)

// 文件日志
type ConsoleLogger struct {
	Prefix     string
	TraceDepth int //跟踪起始深度
	DumpDepth  int //追踪深度
}

func NewConsoleLogger() *ConsoleLogger {
	fl := &ConsoleLogger{
		Prefix:     "[EZ]",
		TraceDepth: 1,
		DumpDepth:  0,
	}
	return fl
}

// Colors
const (
	Reset       = "\033[0m"
	Red         = "\033[31m"
	Green       = "\033[32m"
	Yellow      = "\033[33m"
	Blue        = "\033[34m"
	Magenta     = "\033[35m"
	Cyan        = "\033[36m"
	White       = "\033[37m"
	BlueBold    = "\033[34;1m"
	MagentaBold = "\033[35;1m"
	RedBold     = "\033[31;1m"
	YellowBold  = "\033[33;1m"
)

func (this *ConsoleLogger) Write(data string) {
	pc, f, l, _ := runtime.Caller(this.TraceDepth)
	data = fmt.Sprintf("%s [LOG FILE] %s %s:%d\n%s [LOG FUNC] %s %s\n%s [LOG TEXT] %s\n",
		this.Prefix,
		tools.GetDateYMDHIS("-", ":", " "), f, l,
		this.Prefix,
		tools.GetDateYMDHIS("-", ":", " "),
		runtime.FuncForPC(pc).Name(),
		this.Prefix,
		data,
	)
	rightArrow := ""
	for i := 0; i < this.DumpDepth; i++ {
		pc, f, l, _ = runtime.Caller(this.TraceDepth + i + 1)
		if f == "" {
			break
		}
		rightArrow += "→"
		data = fmt.Sprintf("%s [LOG TRACE]\t%s %s %s:%d FUNC:%s\n",
			this.Prefix,
			tools.GetDateYMDHIS("-", ":", " "),
			rightArrow,
			f, l, runtime.FuncForPC(pc).Name()) + data
	}
	_, e := os.Stdout.WriteString(data)
	PrintError(e)
}

func (this *ConsoleLogger) Print(data string) {
	data = fmt.Sprintf("%s [LOG] %s %s\n", this.Prefix, tools.GetDateYMDHIS("-", ":", " "), data)
	_, e := os.Stdout.WriteString(data)
	PrintError(e)
}
