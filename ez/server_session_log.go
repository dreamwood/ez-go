package ez

import "time"

type SessionLog struct {
	Type    string
	Title   string
	Content string
	Group   string
	Time    time.Duration
}

func (this *Session) AddSessionLog(logType, title, content, group string) {
	this.Logs = append(this.Logs, &SessionLog{
		Type:    logType,
		Title:   title,
		Content: content,
		Group:   group,
		Time:    time.Since(this.StartAt)})
}

const (
	SesionLogTye_Mongodb = "mongodb"
)
