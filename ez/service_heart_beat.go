package ez

import (
	"encoding/json"
	"fmt"
)

//func HeartBeat() {
//	info := ServiceInfo{
//		Service: ConfigService,
//		Server:  ConfigServer,
//		Pid:     os.Getpid(),
//		RunPath: os.Args[0],
//	}
//	err := NsqProducer.Publish(EzTopicHeartBeat, info.ToBytes())
//	if err != nil {
//		LogToConsole(err.Error())
//	}
//}

type Host struct {
	App        string
	Ip         string
	Port       int
	Weight     int
	ApiPrefix  string
	ApiReplace string
}

func (h *Host) String() string {
	return fmt.Sprintf("App:%s,Ip:%s,Port:%d,Weight:%d,ApiPrefix:%s,ApiReplace:%s", h.App, h.Ip, h.Port, h.Weight, h.ApiPrefix, h.ApiReplace)
}

// json序列化Host
func (h *Host) ToJson() []byte {
	content, err := json.Marshal(h)
	if err != nil {
		return nil
	}
	return content
}
func HeartBeat() {
	models := ConfigService.Routers
	for _, model := range models {
		info := Host{
			App:        ConfigService.AppId,
			Ip:         ConfigServer.ServerHost,
			Port:       ConfigServer.ServerPort,
			Weight:     ConfigService.Weight,
			ApiPrefix:  model.Prefix,
			ApiReplace: model.Replace,
		}
		err := NsqProducer.Publish(EzTopicHeartBeat, info.ToJson())
		if err != nil {
			LogToConsole(err.Error())
		}
	}
}
