package ez

import (
	"goftp.io/server/core"
	"goftp.io/server/driver/file"
	"log"
)

func StartFtp(root string, port int, user, pass string) {
	Name := "FTP Server"
	//rootPath := "./static" //FTP根目录
	rootPath := root //FTP根目录
	Port := port     //FTP 端口
	var perm = core.NewSimplePerm("AAA", "BBB")

	// Server options without hostname or port
	opt := &core.ServerOpts{
		Name: Name,
		Factory: &file.DriverFactory{
			RootPath: rootPath,
			Perm:     perm,
		},
		Auth: &core.SimpleAuth{
			Name:     user, // FTP 账号
			Password: pass, // FTP 密码
		},
		Port: Port,
	}
	// start ftp server
	s := core.NewServer(opt)
	err := s.ListenAndServe()
	if err != nil {
		log.Fatal("Error starting server:", err)
	}

}
