package ez

import "strings"

type EzLogger interface {
	Write(text string)
}

func InitConsoleLogger() {
	LogConsole = NewConsoleLogger()
	LogConsole.Prefix = ConfigLog.Prefix
	LogConsole.TraceDepth = 2
}
func LogToConsole(text ...string) {
	LogConsole.Write(strings.Join(text, " "))
}
func LogToConsoleNoTrace(text ...string) {
	LogConsole.Print(strings.Join(text, " "))
}
func InitFileLogger() {
	LogFile = NewFileLogger()
	LogFile.Prefix = ConfigLog.Prefix
	LogFile.TraceDepth = 2
}
func LogToFile(text ...string) {
	LogFile.Write(strings.Join(text, " "))
}
func InitNsqLogger() {
	LogNsq = NewNsqLogger()
	LogNsq.Prefix = ConfigLog.Prefix
	LogNsq.TraceDepth = 2
	LogNsq.NsqProducer = NsqProducer
}
func LogToNsq(text ...string) {
	LogNsq.Write(strings.Join(text, " "))
}
func InitMongoLogger() {
	LogMongo = NewMongoLogger()
	LogMongo.Prefix = ConfigLog.Prefix
	LogMongo.TraceDepth = 2
}
func LogToMongo(text ...string) {
	LogMongo.Write(strings.Join(text, " "))
}
