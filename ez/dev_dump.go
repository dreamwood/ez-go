package ez

import (
	"encoding/json"
	"fmt"
	"gitee.com/dreamwood/ez-go/tools"
	"os"
	"reflect"
	"runtime"
	"time"
)

func DumpToFile(data interface{}) {
	if ConfigCore.IsDev {
		tp := reflect.TypeOf(data)
		row1 := fmt.Sprintf("\ndata type = %s", tp.String())
		filePath := fmt.Sprintf(
			"./var/dumps/%s.log",
			tools.GetDateYMD())
		file, _ := os.OpenFile(filePath, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
		defer file.Close()

		pc, f, l, _ := runtime.Caller(1)
		fileInfo := fmt.Sprintf("\n%s %s %s:%d\n%s %s FUNC:%s\n",
			"[EZ]",
			tools.GetDateYMDHIS("-", ":", " "),
			f, l,
			"[EZ]",
			tools.GetDateYMDHIS("-", ":", " "),
			runtime.FuncForPC(pc).Name(),
		)
		file.Write([]byte(fileInfo))
		file.Write([]byte(time.Now().Format(time.RFC3339)))
		file.Write([]byte(row1))
		file.Write([]byte(fmt.Sprintf("\n%+v", data)))
		file.Write([]byte(fmt.Sprintf("\n%#v", data)))
		content, err := json.MarshalIndent(data, "", "\t")
		if err != nil {
			file.Write([]byte(err.Error()))
		} else {
			file.Write(content)
		}

	}
}
