package ez

import (
	"encoding/json"
	"fmt"
	"runtime"
	"time"
)

func JsonLog(v interface{}) {
	if ConfigCore.IsDev {
		println("#####JSON_LOGGER [" + fmt.Sprintf("%d", time.Now().UnixNano()) + "]")
		for i := 0; i < 3; i++ {
			_, file, line, _ := runtime.Caller(3 - i)
			println(fmt.Sprintf("[%d] %s:%d\t", i+1, file, line))
		}
		switch v.(type) {
		case string:
			println("data type = string")
			println(v.(string))
		case []byte:
			println("data type = []byte", len(v.([]byte)))
			println(string(v.([]byte)))
		default:
			//tp := reflect.TypeOf(v)
			fmt.Printf("data type = unknown ( %#v )", v)
			fmt.Printf("%v\n", v)
			str, e := json.MarshalIndent(v, "", "\t")
			if e != nil {
				println(fmt.Sprintf("[EzErr] %s", e.Error()))
				return
			}
			println(string(str) + "\r\n")
		}
	}
}

func GetLine() int {
	_, _, line, _ := runtime.Caller(1)
	return line
}

func PrintLine() {
	if ConfigCore.IsDev {
		_, _, line, _ := runtime.Caller(1)
		println(fmt.Sprintf("Line:%d", line))
	}
}
func Debug(v interface{}) {
	if ConfigCore.IsDev {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf(fmt.Sprintf("%s:%d\n", file, line))
		fmt.Printf("%v\n", v)
	}
}
func Trace(v interface{}, depth int) {
	if ConfigCore.IsDev {
		for i := 0; i < depth; i++ {
			_, file, line, _ := runtime.Caller(i)
			fmt.Printf(fmt.Sprintf("%s:%d\n", file, line))
		}
		fmt.Printf("%v\n", v)
	}
}
