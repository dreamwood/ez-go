package ez

type HttpQuery struct {
	Conditions map[string]interface{} `json:"_where"`
	Order      []string               `json:"_order"`
	Page       int                    `json:"page"`
	Limit      int                    `json:"limit"`
	Depth      int                    `json:"_d"`
	Group      string                 `json:"_g"`
	Search     string                 `json:"_search"`
	Dumps      []string               `json:"_dumps"`
}

func NewHttpQuery() *HttpQuery {
	return &HttpQuery{
		Conditions: make(map[string]interface{}),
		Order:      make([]string, 0),
		Dumps:      make([]string, 0),
		Page:       1,
		Limit:      10,
	}
}
