package ez

import (
	"fmt"
	"gitee.com/dreamwood/ez-go/tools"
	"github.com/go-redis/redis"
)

type RedisConfig struct {
	Dsn      string `yaml:"dsn"`
	Db       int    `yaml:"document"`
	Password string `yaml:"password"`
}

var ConfigRedis *RedisConfig

func LoadRedisConfig() *RedisConfig {
	tools.CreateConfigFromYml("./app.yaml", "redis", &ConfigRedis)
	return ConfigRedis
}
func InitRedis() {
	LogToConsoleNoTrace("REDIS 开始初始化")
	LogToConsoleNoTrace(fmt.Sprintf("Redis数据库链接Addr：%s", ConfigRedis.Dsn))
	DBRedis = redis.NewClient(&redis.Options{
		Addr:     ConfigRedis.Dsn, // 指定
		Password: ConfigRedis.Password,
		DB:       ConfigRedis.Db, // redis一共16个库，指定其中一个库即可
	})
	go func() {
		_, err := DBRedis.Ping().Result()
		PrintError(err)
	}()
	LogToConsoleNoTrace("REDIS 初始化完成")
}

func GetRedis() *redis.Client {
	if DBRedis == nil {
		InitRedis()
	}
	return DBRedis
}
