package ez

import (
	"fmt"
	"runtime"
	"sync"
)

var printLock sync.Mutex

func init() {
	printLock = sync.Mutex{}
}

func PrintError(e error, other ...string) bool {
	if e != nil {
		if ConfigCore.IsDev {
			printLock.Lock()
			defer printLock.Unlock()
			_, f, l, _ := runtime.Caller(1)
			println(fmt.Sprintf("%s:%d", f, l))
			if len(other) > 0 {
				println("[ERROR]", other[0])
			}
			print("[ERROR]", e.Error())
			println("")
		}
		return true
	} else {
		return false
	}
}
