package ez

const (
	Role_User        = "Role_User"
	Role_Login       = "Role_Login"
	Role_Admin       = "Role_Admin"
	Role_Super_Admin = "Role_Super_Admin"
)

type AccessControl struct {
	AccessRules map[string]AcRule
}

type AcRule struct {
	Route string
	Rules []string
}

var AC *AccessControl

func init() {
	AC = &AccessControl{
		AccessRules: make(map[string]AcRule),
	}
}

func (this *AccessControl) Add(route string, rules ...string) {
	this.AccessRules[route] = AcRule{route, rules}
}
