package ez

type ApiConfig struct {
	//身份认证token名称
	AuthTokenName string `yaml:"auth-token-name"`
	// session进程令牌
	SessionTokenName string `yaml:"session-token-name"`
	TokenExpire      int64  `yaml:"token-expire"`
}

type CoreConfig struct {
	Vi             string `yaml:"vi"`
	IsDev          bool   `yaml:"is-dev"`
	RequestLogOpen bool   `yaml:"request-log-open"`
}

type ServerConfig struct {
	ServerRoot string `yaml:"server-root"`
	ServerHost string `yaml:"server-host"`
	ServerPort int    `yaml:"server-port"`
	// FileServer 静态文件服务器路由和文件夹的配置
	FileServers map[string]string `yaml:"file-servers"`
	//Deteriorated todo 后续版本删除
	UserTLS  bool   `yaml:"user-tls"`
	UseTLS   bool   `yaml:"use-tls"`
	CertFile string `yaml:"cert-file"`
	KeyFile  string `yaml:"key-file"`
}

type LogConfig struct {
	// LogTo 日志输出方式
	// nsq: 输出到nsq
	// file: 输出到文件
	// console: 输出到控制台
	// mgo: 输出到mongodb
	LogTo  string `yaml:"log-to"`
	Prefix string `yaml:"prefix"`
}

type ServiceConfig struct {
	AppId   string `yaml:"app-id"`
	AppName string `yaml:"app-name"`
	// 多负载配置
	MachineCode       string          `yaml:"machine-code"` //设备码
	Weight            int             `yaml:"weight"`       //负载均衡权重
	RouterPrefix      string          `yaml:"router-prefix"`
	RouterReplace     string          `yaml:"router-replace"`
	HeartBeatInterval int             `yaml:"heart-beat-interval"`
	Routers           []ServiceRouter `yaml:"routers"`
}

type ServiceRouter struct {
	Prefix  string `yaml:"pre"`
	Replace string `yaml:"rep"`
}

type ErrorLog struct {
	App     string      `json:"app" bson:"app"`         //APP
	Code    string      `json:"code" bson:"code"`       //错误码
	Message string      `json:"message" bson:"message"` //错误信息
	Data    interface{} `json:"data" bson:"data"`       //错误数据
	State   int64       `json:"state" bson:"state"`     //状态
}

type Pic struct {
	Name string `json:"name"`
	Url  string `json:"url"`
	Size int64  `json:"size"`
}

type Pics []Pic

type File struct {
	Name string `json:"name"`
	Url  string `json:"url"`
	Size int64  `json:"size"`
}

type Files []File
