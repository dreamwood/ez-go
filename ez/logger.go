package ez

//
//type EzLogger struct {
//	Prefix        string
//	Type          string //默认写入文件
//	ConsoleLogger *ConsoleLogger
//	FileLogger    *FileLogger
//	NsqLogger     *NsqLogger
//
//	//配置
//	//-文件日志
//	FilePath string
//	//mq日志
//	NsqPubAddr string
//	NsqTopic   string
//	DepthSkip  int
//}
//
//var logger *EzLogger
//
//func CreateLogger() *EzLogger {
//	root, _ := os.Getwd()
//	logger = &EzLogger{
//		Type:      EzLogToConsole,
//		FilePath:  root + "/var/logs/ez.log",
//		Prefix:    "[EZ]",
//		DepthSkip: 1,
//	}
//	return logger
//}
//
//func (this *EzLogger) InitFileLogger() *FileLogger {
//	//初始化文件日志
//	this.FileLogger = NewFileLogger()
//	if this.FilePath != "" {
//		this.FileLogger.FilePath = this.FilePath
//	}
//	return this.FileLogger
//}
//
//func (this *EzLogger) InitConsoleLogger() *ConsoleLogger {
//	//初始化Console日志
//	this.ConsoleLogger = NewConsoleLogger()
//	return this.ConsoleLogger
//}
//
//func (this *EzLogger) InitNsqLogger() *NsqLogger {
//	//初始化Nsq日志
//	//this.NsqLogger = NewNsqLogger(this.NsqPubAddr, this.NsqTopic)
//	this.NsqLogger = NewNsqLogger()
//	return this.NsqLogger
//}
//
//func (this *EzLogger) UseFileLogger() {
//	this.Type = EzLogToFile
//}
//
//func (this *EzLogger) GetFileLogger() *FileLogger {
//	return this.FileLogger
//}
//
//func (this *EzLogger) UseConsoleLogger() {
//	this.Type = EzLogToConsole
//}
//
//func (this *EzLogger) GetConsoleLogger() *ConsoleLogger {
//	return this.ConsoleLogger
//}
//
//func (this *EzLogger) UseNsqLogger() {
//	this.Type = EzLogToNsq
//}
//
//func (this *EzLogger) GetNsqLogger() *NsqLogger {
//	return this.NsqLogger
//}
//
//func (this *EzLogger) Printf(format string, v ...interface{}) {
//	//调用链跟踪
//	pc, f, l, _ := runtime.Caller(this.DepthSkip)
//	format = format + "\n"
//	prefix := fmt.Sprintf("%s %s %s:%d FUNC:%s \n",
//		this.Prefix,
//		tools.GetDateYMDHIS("-", ":", " "),
//		f, l, runtime.FuncForPC(pc).Name())
//	switch this.Type {
//	case EzLogToNsq:
//		if this.NsqLogger == nil {
//			this.ConsoleLogger.Write(prefix)
//			this.ConsoleLogger.Write(fmt.Sprintf(format, v...))
//		} else {
//			this.NsqLogger.Write(prefix)
//			this.NsqLogger.Write(fmt.Sprintf(format, v...))
//		}
//	case EzLogToConsole:
//		this.ConsoleLogger.Write(prefix)
//		this.ConsoleLogger.Write(fmt.Sprintf(format, v...))
//	case EzLogToFile:
//		this.FileLogger.Write(prefix)
//		this.FileLogger.Write(fmt.Sprintf(format, v...))
//	default:
//		//不输出
//	}
//}
//
//func (this *EzLogger) Write(data string) {
//	//调用链跟踪
//	pc, f, l, _ := runtime.Caller(this.DepthSkip)
//	data = data + "\n"
//	prefix := fmt.Sprintf("%s %s %s:%d FUNC:%s \n",
//		this.Prefix,
//		tools.GetDateYMDHIS("-", ":", " "),
//		f, l, runtime.FuncForPC(pc).Name())
//	switch this.Type {
//	case EzLogToNsq:
//		if this.NsqLogger == nil {
//			this.ConsoleLogger.Write(prefix)
//			this.ConsoleLogger.Write(data)
//		} else {
//			this.NsqLogger.Write(prefix)
//			this.NsqLogger.Write(data)
//		}
//	case EzLogToConsole:
//		this.ConsoleLogger.Write(prefix)
//		this.ConsoleLogger.Write(data)
//	case EzLogToFile:
//		this.FileLogger.Write(prefix)
//		this.FileLogger.Write(data)
//	default:
//		//不输出
//	}
//}
//func (this *EzLogger) OutPut(data string) {
//	this.Write(data)
//}
