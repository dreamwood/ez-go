package ez

import (
	"github.com/go-redis/redis"
	"github.com/nsqio/go-nsq"
	"go.mongodb.org/mongo-driver/mongo"
	"gorm.io/gorm"
)

const (
	EventBeforeConfigReady = "EventBeforeConfigReady"
	EventAfterConfigReady  = "EventAfterLoadConfig"
	//配置完成后才会触发serverRun事件
	EventBeforeServerRun = "EventBeforeServerRun"
	EventAfterServerRun  = "EventAfterServerRun"
	EventPublicKey       = "ez.public.event"
	EventPrivateKey      = "ez.private.event"

	LoggerNsqTopic = "ez.log.nsq"

	EzErrorLogDoc = `ErrLog`
	EzErrorReport = `ErrLog`

	EzPublicTopic     = "ez.public.topic"
	EzPublicChannel   = "ez.public.channel"
	EzTopicReg        = "ez.topic.reg"
	EzTopicRegApi     = "ez.topic.reg.api"
	EzTopicRegModel   = "ez.topic.reg.model"
	EzTopicHeartBeat  = "ez.topic.heart-beat"
	EzTopicOffline    = "ez.topic.offline"
	EzTopicRequestLog = "ez.topic.request-log"
	EzTopicErrorLog   = "ez.topic.error-log"

	EzLogToFile    = "file"
	EzLogToConsole = "console"
	EzLogToNsq     = "nsq"
)

var NsqProducer *nsq.Producer
var NsqConsumer *nsq.Consumer

var DBMysql *gorm.DB
var DBMongo *mongo.Database
var DBRedis *redis.Client

var ConfigServer *ServerConfig
var ConfigApi *ApiConfig
var ConfigCore *CoreConfig
var ConfigService *ServiceConfig
var ConfigLog *LogConfig
var ConfigNsq *NsqConfig
var ConfigMongo *MongoDbConfig
var ConfigMysql *MysqlConfig

var LogConsole *ConsoleLogger
var LogNsq *NsqLogger
var LogMongo *MongoLogger
var LogFile *FileLogger
