package ez

import "sync"

type Lock struct {
	mt sync.Mutex
}

func NewLock() *Lock {
	return &Lock{
		mt: sync.Mutex{},
	}
}

func (l *Lock) Do(f func()) {
	l.mt.Lock()
	f()
	l.mt.Unlock()
}

func (l *Lock) DoSafe(f func()) {
	for !l.mt.TryLock() {
		l.mt.Unlock()
	}
	f()
	l.mt.Unlock()
}
