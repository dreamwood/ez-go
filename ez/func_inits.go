package ez

import (
	"gitee.com/dreamwood/ez-go/tools"
)

func DefaultInit() {

	LoadServerConfig()
	LoadApiConfig()
	LoadCoreConfig()
	LoadLogConfig()
	LoadServiceConfig()

	InitConsoleLogger()
	InitFileLogger()

	LoadNsqConfig()
	InitNsq()
	InitNsqLogger()

	LoadMongoDbConfig()
	InitMongoDb()
	InitMongoLogger()

	LoadMysqlConfig()
	InitMysql()
	//todo MysqlLogger

	LoadRedisConfig()
	InitRedis()
}

func LoadServerConfig() {
	tools.CreateConfigFromYml("./app.yaml", "server", &ConfigServer)
}
func LoadApiConfig() {
	tools.CreateConfigFromYml("./app.yaml", "api", &ConfigApi)
}
func LoadLogConfig() {
	tools.CreateConfigFromYml("./app.yaml", "logger", &ConfigLog)
}
func LoadCoreConfig() {
	tools.CreateConfigFromYml("./app.yaml", "core", &ConfigCore)
}
func LoadServiceConfig() {
	tools.CreateConfigFromYml("./app.yaml", "service", &ConfigService)
}
