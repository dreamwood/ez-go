package ez

import (
	"fmt"
	"gitee.com/dreamwood/ez-go/tools"
	"io/fs"
	"path/filepath"
)

func RegModelsToDev(dir string) {
	if !ConfigCore.IsDev {
		return
	}
	root := fmt.Sprintf("%s/apps/%s/auto/reg/document", ConfigServer.ServerRoot, dir)
	//遍历root目录，找到所有文件
	filepath.Walk(root, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			println(err.Error())
		} else {
			if info.IsDir() {
				return nil
			} else {
				data := tools.ReadFile(path)
				e := NsqProducer.Publish(EzTopicRegModel, data)
				if e == nil {
					println(e)
				}
			}
		}
		return nil
	})
}
