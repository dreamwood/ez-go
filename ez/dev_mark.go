package ez

import "gitee.com/dreamwood/ez-go/tools"

func Mark() string {
	mark := tools.CreateRandString(16)
	mark = "======" + mark + "======"
	println(mark)
	return mark
}
