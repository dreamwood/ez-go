package mgo

type DocConfig struct {
	ContainerKey    string                     //注册到容器中的key
	Fields          []string                   //字段列表
	RelationFields  []string                   //字段列表
	RelationConfigs map[string]*DocRelation    //关联关系
	FieldFilter     map[string]*DocFieldFilter //字段过滤
	ExcelFields     map[string]*ExcelField     //导出字段
}

type DocRelation struct {
	Config       func() *DocConfig
	ContainerKey string ""
	DocName      string
	JoinType     string //M对多,O对单
	KeyInside    string
	KeyOutSide   string
}

type DocFieldFilter struct {
	Select []string
	Omit   []string
}

type ExcelField struct {
	Values []string
	Names  []string
}
