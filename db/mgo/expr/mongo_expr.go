package expr

import "go.mongodb.org/mongo-driver/bson"

func Or(values ...bson.D) bson.D {
	return bson.D{
		{
			OR_C,
			values,
		},
	}
}
func And(values ...bson.D) bson.D {
	return bson.D{
		{
			AND_C,
			values,
		},
	}
}

func ExprX(field string, expr string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					expr,
					value,
				},
			},
		},
	}
}
func Expr(field string, value interface{}) bson.D {
	return bson.D{
		{field, value},
	}
}

func Gt(field string, value interface{}) bson.E {
	return bson.E{
		field,
		bson.D{
			{
				GT_C,
				value,
			},
		},
	}
}
func Gte(field string, value interface{}) bson.E {
	return bson.E{
		field,
		bson.D{
			{
				GTE_C,
				value,
			},
		},
	}
}

func Lt(field string, value interface{}) bson.E {
	return bson.E{
		field,
		bson.D{
			{
				LT_C,
				value,
			},
		},
	}
}
func Lte(field string, value interface{}) bson.E {
	return bson.E{
		field,
		bson.D{
			{
				LTE_C,
				value,
			},
		},
	}
}
func Not(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					NOT_C,
					value,
				},
			},
		},
	}
}
func Eq(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					EQ_C,
					value,
				},
			},
		},
	}
}
func Ne(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					NE_C,
					value,
				},
			},
		},
	}
}
func In(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					IN_C,
					value,
				},
			},
		},
	}
}
func Nin(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					NIN_C,
					value,
				},
			},
		},
	}
}
func Exists(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					EXISTS_C,
					value,
				},
			},
		},
	}
}
func Regex(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					REGEX_C,
					value,
				},
			},
		},
	}
}
func All(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					ALL_C,
					value,
				},
			},
		},
	}
}
func Type(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					TYPE_C,
					value,
				},
			},
		},
	}
}
func Size(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					SIZE_C,
					value,
				},
			},
		},
	}
}

//	func Sum(field string, value interface{}) bson.D {
//		return bson.D{
//			{
//				field,
//				bson.D{
//					{
//						SUM_C,
//						value,
//					},
//				},
//			},
//		}
//	}
func Sum(field string, value interface{}) bson.E {
	return bson.E{
		field,
		bson.D{
			{
				SUM_C,
				value,
			},
		},
	}
}
func Avg(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					AVG_C,
					value,
				},
			},
		},
	}
}
func Min(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					MIN_C,
					value,
				},
			},
		},
	}
}
func Max(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					MAX_C,
					value,
				},
			},
		},
	}
}
func Mod(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					MOD_C,
					value,
				},
			},
		},
	}
}
func Push(field string, value interface{}) bson.E {
	return bson.E{
		field,
		bson.D{
			{
				PUSH_C,
				value,
			},
		},
	}
}
func Pull(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					PULL_C,
					value,
				},
			},
		},
	}
}
func ElemMatch(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					ELEM_MATCH_C,
					value,
				},
			},
		},
	}
}
func Set(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					SET_C,
					value,
				},
			},
		},
	}
}
func Unset(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					UNSET_C,
					value,
				},
			},
		},
	}
}
func Text(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					TEXT_C,
					value,
				},
			},
		},
	}
}
func CurrentDate(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					CURRENT_DATE_C,
					value,
				},
			},
		},
	}
}
func CurrentTimestamp(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					CURRENT_TIMESTAMP_C,
					value,
				},
			},
		},
	}
}
func Where(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					WHERE_C,
					value,
				},
			},
		},
	}
}
func Inc(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					INC_C,
					value,
				},
			},
		},
	}
}
func Mul(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					MUL_C,
					value,
				},
			},
		},
	}
}
func Rename(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					RENAME_C,
					value,
				},
			},
		},
	}
}
func Pop(field string, value interface{}) bson.D {
	return bson.D{
		{
			field,
			bson.D{
				{
					POP_C,
					value,
				},
			},
		},
	}
}
func First(field string, value interface{}) bson.E {
	return bson.E{
		field,
		bson.D{
			{
				FIRST_C,
				value,
			},
		},
	}
}
func Last(field string, value interface{}) bson.E {
	return bson.E{
		field,
		bson.D{
			{
				LAST_C,
				value,
			},
		},
	}
}

func Count(field string) bson.D {
	return bson.D{
		{
			COUNT_C,
			field,
		},
	}
}
