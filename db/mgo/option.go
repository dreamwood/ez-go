package mgo

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func Option(offset int, limit int, orderBy ...string) *options.FindOptions {
	//处理排序数据
	realOrderBy := bson.D{}
	for _, str := range orderBy {
		if str[0:1] == "-" || str[0:1] == "_" {
			realOrderBy = append(realOrderBy, bson.E{str[1:], -1})
		} else {
			realOrderBy = append(realOrderBy, bson.E{str, 1})
		}
	}
	opt := options.FindOptions{}
	return opt.SetLimit(int64(limit)).SetSkip(int64(offset)).SetSort(realOrderBy)
}

func OptionOne(orderBy ...string) *options.FindOneOptions {
	//处理排序数据
	realOrderBy := bson.D{}
	for _, str := range orderBy {
		if str[0:1] == "-" {
			realOrderBy = append(realOrderBy, bson.E{str[1:], -1})
		} else {
			realOrderBy = append(realOrderBy, bson.E{str, 1})
		}
	}
	opt := options.FindOneOptions{}
	return opt.SetSort(realOrderBy)
}
