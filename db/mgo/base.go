package mgo

type BaseDoc struct {
	Factory *Factory
}

func (this *BaseDoc) SetEvent(isOn bool) *BaseDoc {
	this.GetFactory().IsEventOn = isOn
	return this
}

func (this *BaseDoc) SetFactoryParams(argus ...interface{}) *BaseDoc {
	this.GetFactory().SetArgus(argus...)
	return this
}

func (this *BaseDoc) GetFactory() *Factory {
	if this.Factory == nil {
		this.Factory = NewFactory()
	}
	return this.Factory
}
