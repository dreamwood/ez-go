package pool

import (
	"testing"
	"time"
)

func TestNewPool(t *testing.T) {
	c := make(chan int, 10)
	closeCh := make(chan int, 10)

	go func() {
		for {
			select {
			case a := <-c:
				println("print rt1", a)
				for i := 0; i < 1000000000; i++ {
					print('A')
					time.Sleep(100 * time.Millisecond)
				}
				time.Sleep(100 * time.Second)
				println("unreachable")
			case <-closeCh:
				println("close")
				return
			}
		}
	}()
	c <- 1
	c <- 2
	c <- 3
	c <- 4
	time.Sleep(1 * time.Second)
	closeCh <- 1

}
