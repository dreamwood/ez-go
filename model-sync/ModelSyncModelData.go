package sync

import "encoding/json"

type ModelData struct {
	PubName   string
	ModelName string
	Id        int
	Crud      string //增删改查的类型
	Data      []byte
}

func (this *ModelData) Set(data []byte) *ModelData {
	this.Data = data
	return this
}

func (this *ModelData) SetString(data string) *ModelData {
	this.Data = []byte(data)
	return this
}

func (this *ModelData) SetAndJson(data interface{}) *ModelData {
	content, e := json.Marshal(data)
	if e != nil {
		println(e)
		this.Data = []byte(e.Error())
		return this
	}
	this.Data = content
	return this
}

func (this *ModelData) FillModel(model interface{}) error {
	return json.Unmarshal(this.Data, &model)
}
