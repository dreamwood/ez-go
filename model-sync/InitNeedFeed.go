package sync

import (
	"context"
	"fmt"
	"gitee.com/dreamwood/ez-go/ez"
)

type InitNeed struct {
	SubName   string
	ModelName string
}

type InitFeed struct {
	Need *InitNeed
	Data *ModelData
}

func (ms *ModelSync) ListenToInitNeed(modelName string, handler func(*InitNeed)) {
	ez.Subscribe(fmt.Sprintf("MsPub.SubInitNeed.%s", modelName), func(v interface{}, ctx context.Context) {
		in, ok := v.(*InitNeed)
		if ok {
			handler(in)
		}
	})
}

func InitNeedFeedSub() {
	//创建初始化监听,发布方被动响应的行为。
	//使用场景如下：
	//当某个服务初始化时，需要初次拉取数据（或者因为某些原因需要重新拉取）时
	//订阅方主动发布PubInitNeed，告知发布方，我需要初始化数据，给爹送过来
	//发布方在SubInitNeed监听到这个事件，通过FeedNeeds（这里需要根据自己
	//的场景）自行实现，在FeedNeeds中，发布方应该调用FeedToInitEvent，把全部
	//需要传递的数据逐条发布过去，订阅方在SubInitFeed，会收到“数据来了，接
	//好了，孙贼！”，大量数据的涌来有可能让孙子崩溃，所以要么发布方做好数据分
	//批发送，要么订阅方自己做好分批缓冲操作。
	//事件流程如下：
	//1.发布方监听，执行SubInitNeed
	//2.订阅方监听，执行SubInitFeed
	//3.订阅方发起，执行PubInitNeed
	//4.发布方在SubInitNeed的回调中执行FeedToInitEvent
	//5.订阅方在SubInitFeed的回调中执行数据同步
	ez.Try(MS.SubInitNeed(func(in *InitNeed) error {
		//通过event发布消息到内部
		ez.DispatchToMany(fmt.Sprintf("MsPub.SubInitNeed.%s", in.ModelName), in, context.TODO())
		return nil
	}))
}
