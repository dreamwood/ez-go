package ezc

import (
	"fmt"
	"gitee.com/dreamwood/ez-go/ez"
)

const (
	Action_List         = "list"
	Action_Get          = "get"
	Action_Scan         = "scan"
	Action_Choice       = "choice"
	Action_Update       = "update"
	Action_Save         = "save"
	Action_Save_Many    = "save_many"
	Action_Delete       = "delete"
	Action_UnDelete     = "undelete"
	Action_Delete_Many  = "delete_many"
	Action_Destroy      = "destroy"
	Action_Destroy_Many = "destroy_many"
	Action_Copy         = "copy"
	Action_Export       = "export"
	Action_Import       = "import"
	Action_Help         = "help"
	Action_Tree         = "tree"
)

type BaseAdminController struct {
	Group  string
	Prefix string
	Model  string
}

func (this *BaseAdminController) SetRouteParam(group, prefix, model string) {
	this.Group = group
	this.Prefix = prefix
	this.Model = model
}

func (this *BaseAdminController) R(url string) *ez.Router {
	return &ez.Router{Url: url}
}

func (this *BaseAdminController) Api(url string) *ez.Router {
	return &ez.Router{Group: this.Group, Url: url}
}

func (this *BaseAdminController) AdminApi(url string) *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: url}
}

func (this *BaseAdminController) AdminModelApi(action string) *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, action)}
}

func (this *BaseAdminController) AdminList() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_List)}
}
func (this *BaseAdminController) AdminGet() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_Get)}
}
func (this *BaseAdminController) AdminScan() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_Scan)}
}
func (this *BaseAdminController) AdminChoice() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_Choice)}
}
func (this *BaseAdminController) AdminSave() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_Save)}
}
func (this *BaseAdminController) AdminUpdate() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_Update)}
}
func (this *BaseAdminController) AdminUpdateMany() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_Save_Many)}
}
func (this *BaseAdminController) AdminDelete() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_Delete)}
}
func (this *BaseAdminController) AdminUnDelete() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_UnDelete)}
}
func (this *BaseAdminController) AdminDeleteMany() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_Delete_Many)}
}
func (this *BaseAdminController) AdminDestroy() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_Destroy)}
}
func (this *BaseAdminController) AdminDestroyMany() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_Destroy_Many)}
}
func (this *BaseAdminController) AdminCopy() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_Copy)}
}
func (this *BaseAdminController) AdminExport() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_Export)}
}
func (this *BaseAdminController) AdminImport() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_Import)}
}
func (this *BaseAdminController) AdminHelp() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_Help)}
}

func (this *BaseAdminController) AdminTree() *ez.Router {
	return &ez.Router{Group: this.Group, Prefix: this.Prefix, Url: fmt.Sprintf("/%s/%s", this.Model, Action_Tree)}
}
